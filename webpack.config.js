const path = require ("path");

module.exports = {
    mode : "development",
    entry : "./src/main.ts",
    output : {
        filename : "app.js",
        path : path.resolve (__dirname, "dist")
    },
    module: {
        rules: [
            {
                test: /\.ts$/,
                use: 'ts-loader',
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: ['.ts', '.js'],
    },
    stats : {
        colors : true
    },
    devtool : "source-map",
    optimization : {
        minimize : true,
        usedExports : true,
        sideEffects: true
    },
    watch : false,
    watchOptions : {
        ignored : /node_modules/,
        aggregateTimeout : 10000
    },
    devServer : {
        contentBase : path.join (__dirname, 'dist'),
        port : 3000
    }
};
